# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer:
pkgname=py3-fonttools
_pkgname=fonttools
pkgver=4.31.1
pkgrel=0
pkgdesc="Converts OpenType and TrueType fonts to and from XML"
url="https://github.com/fonttools/fonttools"
arch="all"
license="MIT AND OFL-1.1"
depends="py3-lxml py3-fs cython python3-dev"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-brotli"
subpackages="$pkgname-doc"
source="$_pkgname-$pkgver.tar.gz::https://github.com/fonttools/fonttools/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-fonttools" # Backwards compatibility
provides="py-fonttools=$pkgver-r$pkgrel" # Backwards compatibility

prepare() {
	default_prepare
	# remove interpreter line
	sed -i '1d' Lib/fontTools/mtiLib/__init__.py
}

build() {
	python3 setup.py build
}

check() {
	PATH="$PATH:$PWD" PYTHONPATH=$(echo "$PWD"/build/lib*) py.test-3
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

doc() {
	replaces="py-$_pkgname-doc" # Backwards compatibility
	provides="py-$_pkgname-doc=$pkgver-r$pkgrel" # Backwards compatibility
	default_doc
}

sha512sums="
cdf70f376e690ab1edc5e761ba45d1ddd7760c3aefdb5e9b88cc97118edd4ff694789c8b79ece791b4b6a597dcac1abb0eec5f0634f1db0792e5e62a2137282d  fonttools-4.31.1.tar.gz
"
